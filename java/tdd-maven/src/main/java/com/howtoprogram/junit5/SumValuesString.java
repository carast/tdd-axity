package com.howtoprogram.junit5;

public class SumValuesString {
    private SumValuesString() {
        throw new IllegalStateException("Utility class");
    }

    public static int sum(String numbers) {
        String[] separateValues = numbers.split("\n");
        String separator = separateValues[0];
        String[] separatorA = separator.split("//");
        String[] numbersArray = separateValues[1].split(separatorA[1]);
        int resultSum = 0;
        if (numbersArray.length == 1 && numbersArray[0] == "") {
            resultSum = 0;
        } else if (numbersArray.length == 1) {
            resultSum = Integer.parseInt(numbersArray[0]) + 0;
        } else if (numbersArray.length == 2) {
            resultSum = Integer.parseInt(numbersArray[0]) + Integer.parseInt(numbersArray[1]);
        } else if (numbersArray.length >= 3) {

            for (int x = 0; x < numbersArray.length; x++) {
                resultSum += Integer.parseInt(numbersArray[x]);
            }
        }
        return resultSum;
    }

}
