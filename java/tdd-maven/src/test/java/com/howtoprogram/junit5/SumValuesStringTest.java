package com.howtoprogram.junit5;

import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(JUnitPlatform.class)
public class SumValuesStringTest {
    @Test
    void testStringEmpty() {
        String numbers = "";
        assertEquals(SumValuesString.sum(numbers), 0);
    }

    @Test
    void testStringOne() {
        String numbers = "1";
        assertEquals(SumValuesString.sum(numbers), 1);
    }

    @Test
    void testStringSum() {
        String numbers = "2,2";

        assertEquals(SumValuesString.sum(numbers), 4);
    }

    @Test
    void testStringSumAmountNumbers() {
        String numbers = "2,2,2,6";

        assertEquals(SumValuesString.sum(numbers), 12);
    }

    @Test
    void testStringSumAmountNumbersLine() {
        String numbers = "2,2,2\n1";

        assertEquals(SumValuesString.sum(numbers), 7);
    }
    @Test
    void testStringSumDelimiter() {
        String numbers = "//;\n2;2;2";

        assertEquals(SumValuesString.sum(numbers), 6);
    }

}
